using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;
using System.Collections;

public class BannerAd : Singleton<BannerAd> {

    public const int MaxHeight = 90;
    public UnityEvent OnBannerShown { get; } = new UnityEvent();
    public bool IsShowing { get; private set; } = false;

    [SerializeField] BannerPosition _bannerPosition = BannerPosition.BOTTOM_CENTER;
    [SerializeField] string _adUnitId = "Banner_Android";

    void Start() {
        Advertisement.Banner.SetPosition(_bannerPosition);
        LoadBanner();
    }

    void LoadBanner() {
        BannerLoadOptions options = new BannerLoadOptions {
            loadCallback = OnBannerLoaded,
            errorCallback = OnBannerError
        };
        Advertisement.Banner.Load(_adUnitId, options);
    }

    void OnBannerLoaded() {
        Debug.Log("Banner ad loaded");
        ShowBannerAd();
    }

    void OnBannerError(string message) {
        Debug.Log($"Banner Error: {message}");
        StartCoroutine(LoadBannerAfterTime());
    }

    IEnumerator LoadBannerAfterTime() {
        const float sleepTime = 5.0f;
        yield return new WaitForSecondsRealtime(sleepTime);
        LoadBanner();
    }

    void ShowBannerAd() {
        BannerOptions options = new BannerOptions {
            showCallback = InnerOnBannerShown,
            hideCallback = OnBannerHidden
        };
        if (Advertisement.Banner.isLoaded) {
            Advertisement.Banner.Show(_adUnitId, options);
        }
    }

    void InnerOnBannerShown() {
        IsShowing = true;
        OnBannerShown.Invoke();
    }

    void OnBannerHidden() {
        IsShowing = false;
        LoadBanner();
    }
}
