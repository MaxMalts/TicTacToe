﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class WifiMultiplayerHelpButtonController : MonoBehaviour {

	[TextArea(3, 10)] public string helpText;

	public async void OnClick() {
		ButtonPopupController popup = PopupsManager.Instance.ShowConfirmPopup(helpText);
		await popup.WaitForClickOrCloseAsync();
		popup.Close();
	}
}
