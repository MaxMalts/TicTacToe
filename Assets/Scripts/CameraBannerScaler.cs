using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraBannerScaler : MonoBehaviour {

    void Start() {
        if (BannerAd.Instance.IsShowing) {
            AdjustHeight();
        } else {
            BannerAd.Instance.OnBannerShown.AddListener(OnBannerShown);
        }
    }

    void OnBannerShown() {
        AdjustHeight();
    }

    void AdjustHeight() {
        Camera camera = GetComponent<Camera>();
        float y = (float) BannerAd.MaxHeight / Screen.height;
        camera.rect = new Rect(0.0f, y, 1.0f, 1 - y);
    }
}
