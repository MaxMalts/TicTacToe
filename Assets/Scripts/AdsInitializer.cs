using UnityEngine;
using UnityEngine.Advertisements;

public class AdsInitializer : MonoBehaviour, IUnityAdsInitializationListener {

    private const float initializationCheckInterval = 5f;

    [SerializeField] string _androidGameId;
    [SerializeField] bool _testMode = true;
    private string _gameId;
    private float _timeSinceLastInitialization = 0f;

    void Awake() {
#if UNITY_EDITOR
        _testMode = true;
#endif
        _gameId = _androidGameId;
        
        if (Advertisement.isSupported) {
            Advertisement.Initialize(_gameId, _testMode, this);
        }
    }

    void Update() {
        _timeSinceLastInitialization += Time.deltaTime;
        if (!Advertisement.isInitialized
            && Advertisement.isSupported
            && _timeSinceLastInitialization > initializationCheckInterval) {

            Advertisement.Initialize(_gameId, _testMode, this);
            _timeSinceLastInitialization = 0;
        }
    }

    public void OnInitializationComplete() {
        Debug.Log("Unity Ads initialization complete.");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message) {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }
}